use num::{Integer, Num, Zero};
use fraction::{Fraction, GenericFraction};
use image;
use crate::scalar_field::ScalarField;
use std::collections::{HashMap, VecDeque};
use std::fmt::Debug;

/// The marching squares input here does not handle the double line cases (saddle) properly,
/// as i'm lazy, it's considered an edge case and fully closed paths are not needed.


enum SquareSide {
  Top,
  Right,
  Left,
  Bottom
}
type Point = (f64, f64);
type Segment = (Point, Point);
pub(crate) type Line = Vec<Point>;

// a square of neighbouring elements, i.e. 4 total
type SquareValues<T> = (T, T, T, T);

#[derive(Debug)]
pub struct SquareIterator<'a, T:std::fmt::Debug> {
  pub field: &'a ScalarField<'a, T>,
  pub size: u32,
  pub index: u32
}

#[derive(Debug, Copy, Clone)]
pub struct Square<T> {
  pub values: SquareValues<T>,
  pub position: (u32, u32),
}
//pub type Coordinates = Num + Ord + Copy + Debug + Into<f64>;

pub trait FieldValue: Num + Ord + Copy + Debug + Into<f64> {}
impl<T> FieldValue for T where T: Num + Ord + Copy + Debug + Into<f64> {}

impl<'a, T: Copy + std::fmt::Debug> Iterator for SquareIterator<'a, T> {
  type Item = Square<T>;
  fn next(&mut self) -> Option<Self::Item>{
    if self.index >= self.size {
      return None
    }

    let index00 = self.index;
    let row_index = index00 / self.field.row_len;
    let column_index = index00 %  self.field.row_len;
    let index10 = if column_index < self.field.row_len - 1 {index00 + 1} else {index00};
    let index01 = if row_index < (self.field.column_len - 1) {index00 + self.field.row_len } else { index00 };
    let index11 = if column_index < (self.field.row_len - 1) {index01 + 1} else {index01};

    self.index += 1;
    let values = self.field.values;
    let square = (values[index00 as usize],
                  values[index10 as usize],
                  values[index01 as usize],
                  values[index11 as usize]);

    let res = Square{
      values: square,
      position: (column_index.into(), row_index.into())
    };
    //eprintln!("iterator res {:?}", res);
    Some(res)
  }
}
pub fn get_shade<T: Integer>(square: SquareValues<T>) -> Vec<u8>{
  if square.0 < square.1 {
    return vec![u8::max_value()];
  }
  vec![0]
}

pub fn get_color_shade<T: Integer>(square: SquareValues<T>) -> Vec<u8>{
  if square.0 < square.1 || square.0 < square.2 {
    return vec![u8::max_value(), 0, 0];
  }
  if square.0 > square.1 || square.0 > square.2 {
    return vec![0, 0, u8::max_value()];
  }
  vec![0, 0, 0]
}


pub fn slope_case_from_iso<T: Ord>(square: &SquareValues<T>, iso_height: T) -> u8{
  // default marching squares algorithm
  let mut case = 0;
  if square.0 < iso_height {
    case += 8;
  }
  if square.1 < iso_height {
    case += 4;
  }
  if square.3 < iso_height {
    case += 2;
  }
  if square.2 < iso_height {
    case += 1;
  }
  case
}

fn interpolate<T: FieldValue>(a: T, b: T, middle: T) -> f64{
  let low = a.min(b);
  let high = a.max(b);
  let res = (middle - low).into() / (high - low).into();
  //println!("interpolation res {:?}, values {:?}", res, (a, b, middle));
  if a < b { res } else { 1.0 - res }
}
pub fn contour_segments_clockwise<T: FieldValue>(square: &Square<T>, iso_height: T, case: u8) -> Vec<Segment>{
  let values = square.values;
  let pos = square.position;
  // the border-ignoring position offset works since we pad with equal values
  // so that the square sides reaching across the actual field border are empty anyway
  let top = || (
    pos.0 as f64 + interpolate(values.0, values.1, iso_height),
    pos.1 as f64);
  let right = || (
    (pos.0 + 1) as f64,
    pos.1 as f64 + interpolate(values.1, values.3, iso_height));
  let bottom= ||
      (pos.0 as f64 + interpolate(values.2, values.3, iso_height),
       (pos.1 + 1) as f64);
  let left = || (
    pos.0 as f64,
    pos.1 as f64 + interpolate(values.0, values.2, iso_height));

  match case {
    0 | 15 => { vec![] }
    1 => { vec![(bottom(), left())] }
    2 => { vec![(right(), bottom())] }
    3 => { vec![(right(), left())] }
    4 => { vec![(top(), right())] }
    5 => { vec![(top(), left()), (bottom(), right())]}
    6 => { vec![(top(), bottom())] }
    7 => { vec![(top(), left())] }
    8 => { vec![(left(), top())] }
    9 => { vec![(bottom(), top())] }
    10 => { vec![(left(), bottom()), (right(), top())]}
    11 => { vec![(right(), top())] }
    12 => { vec![(left(), right())] }
    13 => { vec![(bottom(), right())] }
    14 => { vec![(left(), bottom())] }
    _ => panic!("impossible case value")
  }
}

pub fn clockwise_coordinate_change(case: u8) -> (i64, i64){
  let left = ( -1, 0);
  let right = ( 1, 0);
  let up = (0, -1);
  let down = (0, 1);
  match case {
    0 | 15 => {(0,  0)}
    1  => { left }
    2  => { down }
    3  => { left }
    4  => { right }
    5  => { left } //
    6  => { down }
    7  => { left }
    8  => { up }
    9  => { up }
    10 => { up } //
    11 => { up }
    12 => { right }
    13 => { right }
    14 => { down }
    _ => panic!("impossible case value")
  }
}

pub fn counter_clockwise_coordinate_change(case: u8) -> (i64, i64){
  let left = ( -1, 0);
  let right = ( 1, 0);
  let up = (0, -1);
  let down = (0, 1);
  match case {
    0 | 15 => {(0,  0)}
    1  => { down }
    2  => { right }
    3  => { right }
    4  => { up }
    5  => { up }
    6  => { up }
    7  => { up }
    8  => { left }
    9  => { down }
    10 => { right }
    11 => { right }
    12 => { left }
    13 => { down }
    14 => { left }
    _ => panic!("impossible case value")
  }
}

pub fn contour_level<T: FieldValue>(field: &ScalarField<T>, iso_height: T) -> Vec<Line>{
  let mut segment_map: HashMap<(i64, i64), (u8, Vec<Segment>)> = HashMap::new();
  let mut square_to_segment_map = |square: &Square<T>| {
    let case = slope_case_from_iso(&square.values, iso_height);
    let segments = contour_segments_clockwise(&square, iso_height, case);
    if segments.len() > 0 {
      segment_map.insert((square.position.0.clone() as i64, square.position.1.clone() as i64), (case, segments));
    }
  };

  for square in field.square_iter(){
    square_to_segment_map(&square);
  }
  let mut contours: Vec<Line> = vec![];
  let keys = segment_map.keys().cloned().collect::<Vec<_>>();
  //println!("keys {:?}", keys);
  //for k input segment_map.keys().cloned() {
  //  //println!("item {:?}, {:?}", k, segment_map.get(&k));
  //}

  let mut pair_add = |a: (i64, i64), b:(i64, i64)| {(a.0 + b.0, a.1 + b.1)};
  keys.iter().for_each(|key|{
    if let Some((case, segments)) = segment_map.get(&key) {
      let case = *case;
      let mut contour: VecDeque<Point> = VecDeque::new();
      contour.push_back(segments[0].0);

      let mut current_search_key =  pair_add(*key, clockwise_coordinate_change(case));
      while let Some((case1, segments)) = segment_map.get(&current_search_key){
        //println!("cw - start key = {:?}, current key = {:?}, case = {:?}", key, current_search_key, case1);
        let case1 = *case1;
        contour.push_back(segments[0].1);
        segment_map.remove(&current_search_key);
        current_search_key = pair_add(current_search_key, clockwise_coordinate_change(case1));
        //println!("next key = {:?}", current_search_key);
      }

      current_search_key = pair_add(*key, counter_clockwise_coordinate_change(case));;
      while let Some((case2, segments)) = segment_map.get(&current_search_key){
        //println!("ccw - start key = {:?}, current key = {:?}, case = {:?}", key, current_search_key, case2);
        let case2 = *case2;
        contour.push_front(segments[0].0);
        segment_map.remove(&current_search_key);
        current_search_key = pair_add(current_search_key, counter_clockwise_coordinate_change(case2));
        //println!("next key = {:?}", current_search_key);
      }
      segment_map.remove(key);

      //println!("cur contour len = {:?}", contour.len());
      //println!("cur contour = {:?}", contour);
      if contour.len() >= 2 {
        contours.push(contour.into());
      }
      //clockwise_map.remove(&key);
    }
  });
  //println!("contours len = {:?}", contours.len());
  contours
}