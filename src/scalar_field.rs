use crate::square::SquareIterator;

#[derive(Debug)]
pub struct ScalarField<'a, T:std::fmt::Debug> {
  pub values: &'a Vec<T>,
  pub column_len: u32,
  pub row_len: u32,
}

impl<T:std::fmt::Debug> ScalarField<'_, T>{
  pub fn iter(&self) -> ScalarFieldIterator<T>{
    ScalarFieldIterator{
      field: &self,
      size: self.row_len as usize * self.column_len as usize,
      index: 0,
    }
  }
  pub fn square_iter(&self) -> SquareIterator<T>{
    SquareIterator{
      field: &self,
      size: self.row_len * self.column_len,
      index: 0,
    }
  }
}

#[derive(Debug)]
pub struct ScalarFieldIterator<'a, T:std::fmt::Debug> {
  field: &'a ScalarField<'a, T>,
  size: usize,
  index: usize
}

impl<'a, T: Copy + std::fmt::Debug> Iterator for ScalarFieldIterator<'a, T> {
  type Item = (usize, usize, T);
  fn next(&mut self) -> Option<Self::Item>{
    if self.index >= self.size{
      return None
    }
    let elem = self.field.values[self.index as usize];
    let result = Some((self.index % self.field.row_len as usize, self.index / self.field.column_len as usize, elem));
    self.index += 1;
    result
  }
}

